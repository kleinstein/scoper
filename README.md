[![](http://cranlogs.r-pkg.org/badges/grand-total/scoper)](https://www.r-pkg.org/pkg/scoper)
[![](https://cranlogs.r-pkg.org/badges/scoper)](https://www.r-pkg.org/pkg/scoper)
[![](https://img.shields.io/static/v1?label=AIRR-C%20sw-tools%20v1&message=compliant&color=008AFF&labelColor=000000&style=plastic)](https://docs.airr-community.org/en/stable/swtools/airr_swtools_standard.html)

!!! important "2025 Immcantation Users Group Meeting"
    *Are you an Immcantation user and/or interested in adaptive immune receptor repertoire analysis?*
    
    Register now for the upcoming Immcantation Users Group Meeting!
    It will be held virtually on **January 30th, 2025, from 10 to 1:30pm (ET)**.
    All talks will be from user-submitted abstracts.

    Full information here: [https://immcantation.github.io/users-meeting/](https://immcantation.github.io/users-meeting/)
    
**IMPORTANT!** 
SCOPer has moved to https://github.com/immcantation/scoper

To update Git configuration settings use:

```
   git config user.email "your-gh-user@email.com"
   git config user.name "your-gh-user-name"
   git remote set-url origin git@github.com:immcantation/scoper.git
```    

SCOPer
-------------------------------------------------------------------------------

SCOPer (Spectral Clustering for clOne Partitioning) provides a computational framework for the identification of B cell 
clonal relationships from Adaptive Immune Receptor Repertoire sequencing 
(AIRR-Seq) data. It includes methods for assigning clonal identifiers using
sequence identity, hierarchical clustering, and spectral clustering.
SCOPer is part of the [Immcantation](http://immcantation.readthedocs.io) 
analysis framework.

Contact
-------------------------------------------------------------------------------

For help and questions please contact the 
[Immcantation Group](mailto:immcantation@googlegroups.com)
